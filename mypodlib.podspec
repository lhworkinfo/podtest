#
#  Be sure to run `pod spec lint mypodlib.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see https://guides.cocoapods.org/syntax/podspec.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |spec|
  spec.name         = "mypodlib"
  spec.version      = "0.0.1"
  spec.homepage     = "http://EXAMPLE/mypodlib"
  spec.source = { :git => 'https://gitlab.com/lhworkinfo/podtest.git',:tag => spec.version.to_s }
  spec.license = 'MIT'
  spec.dependency 'FMDB', '~> 2.0' 
  spec.author       = { "Longhai Jin" => "lhworkinfo@gmail.com" }
  spec.source_files  = "Classes", "Classes/**/*.{h,m}"
  spec.exclude_files = "Classes/Exclude"
  spec.summary = 'This is pod lib sample.'
end
